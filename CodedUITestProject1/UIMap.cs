﻿namespace CodedUITestProject1
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

    public partial class UIMap
    {
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }

        /// <summary>
        /// SimpleAppTest - Используйте "SimpleAppTestParams" для передачи параметров в этот метод.
        /// </summary>
        public void ModifiedAppTest()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            WinButton uICloseButton = this.UIMainWindowWindow1.UICloseButton;
            #endregion

            // Запуск "E:\c#\SimpleWPFApp\SimpleWPFApp\bin\Debug\SimpleWPFApp.exe"
            ApplicationUnderTest uIMainWindowWindow = ApplicationUnderTest.Launch(this.SimpleAppTestParams.UIMainWindowWindowExePath, this.SimpleAppTestParams.UIMainWindowWindowAlternateExePath);

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(29, 10));

            // Выбор "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTestParams.UICheckBoxCheckBoxChecked;

            // Щелкните "Close" кнопка
            Mouse.Click(uICloseButton, new Point(29, 6));
        }

        public virtual SimpleAppTestParams SimpleAppTestParams
        {
            get
            {
                if ((this.mSimpleAppTestParams == null))
                {
                    this.mSimpleAppTestParams = new SimpleAppTestParams();
                }
                return this.mSimpleAppTestParams;
            }
        }

        private SimpleAppTestParams mSimpleAppTestParams;
    }
    /// <summary>
    /// Параметры для передачи в "SimpleAppTest"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "15.0.26208.0")]
    public class SimpleAppTestParams
    {

        #region Fields
        /// <summary>
        /// Запуск "E:\c#\SimpleWPFApp\SimpleWPFApp\bin\Debug\SimpleWPFApp.exe"
        /// </summary>
        public string UIMainWindowWindowExePath = "E:\\c#\\SimpleWPFApp\\SimpleWPFApp\\bin\\Debug\\SimpleWPFApp.exe";

        /// <summary>
        /// Запуск "E:\c#\SimpleWPFApp\SimpleWPFApp\bin\Debug\SimpleWPFApp.exe"
        /// </summary>
        public string UIMainWindowWindowAlternateExePath = "E:\\c#\\SimpleWPFApp\\SimpleWPFApp\\bin\\Debug\\SimpleWPFApp.exe";

        /// <summary>
        /// Выбор "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = true;
        #endregion
    }
}
